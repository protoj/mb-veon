
-record(mb_screen, {
    screen_id      :: binary(),
    imdbId         :: binary(),
    movieTitle     :: binary(),
    availableSeats :: non_neg_integer(),
    reservedSeats  :: non_neg_integer(),
    created_at  = erlang:system_time(seconds) :: non_neg_integer(),
    updated_at  = erlang:system_time(seconds) :: non_neg_integer()
}).
