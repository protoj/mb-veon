-module(mb_screen_handler).

-include_lib("mixer/include/mixer.hrl").
-include("mb.hrl").

-mixin([
    {mb_base_handler,[
        init/3,
        allowed_methods/2,
        content_types_accepted/2,
        content_types_provided/2,
        resource_exists/2
    ]}
]).
-export([rest_init/2]).
-export([handle_post/2]).
-export([handle_get/2]).
-export([trails/0]).

-type state() :: #{}.

-spec rest_init(cowboy_req:req(), #{}) ->
    {ok, cowboy_req:req(), state()}.
rest_init(Req, Opts) ->
    {Path, _} = cowboy_req:path(Req),
    Action =
        case binary:match(Path, <<"reserve">>) of
            {_, _} -> reserve;
            _      -> screen
        end,
    {ok, Req, #{opts => Opts, action => Action}}.

-spec trails() -> trails:trails().
trails() ->
    RequestBody = #{
        name => <<"request body">>,
        in => body,
        description => <<"request body (as json)">>,
        required => true
    },

    Metadata = #{
        post => #{
            tags => ["Screen"],
            description => "Create new screen for IMDB movie. Reserver a seat for screen",
            consumes => ["application/json"],
            produces => ["application/json"],
            parameters => [RequestBody]
        },
        get => #{
            tags => ["Screen"],
            description => "Get list of screens",
            produces => ["application/json"]
        }
    },

    PostsPath0 = "/screen/reserve",
    PostsPath1 = "/screen/",

    Opts0 = #{
        path => PostsPath0,
        verbose => true
    },
    Opts1 = #{
        path => PostsPath1,
        verbose => true
    },
    [
        trails:trail(PostsPath1, ?MODULE, Opts1, Metadata),
        trails:trail(PostsPath0, ?MODULE, Opts0, maps:remove(get, Metadata))
    ].

-spec handle_post(cowboy_req:req(), state()) ->
    {halt | {boolean(), binary()}, cowboy_req:req(), state()}.
handle_post(CowboyReq, #{action := screen} = State) ->
    lager:notice("~p: handle_post screen", [?MODULE]),
    {ok, Body, CowboyReq1} = cowboy_req:body(CowboyReq),
    case catch mb_util:json_decode(Body) of
        #{<<"imdbId">> := ImdbId, <<"availableSeats">> := AvailableSeats, <<"screenId">> := ScreenId} ->
            Resp = mb_screen:new(ScreenId, ImdbId, AvailableSeats),
            CowboyReq2 = cowboy_req:set_resp_body(mb_util:json_encode(Resp), CowboyReq1),
            {true, CowboyReq2, State};
        _ ->
            {false, CowboyReq1, State }
    end;

handle_post(CowboyReq, #{action := reserve} = State) ->
    lager:notice("~p: handle_post reserve", [?MODULE]),
    {ok, Body, CowboyReq1} = cowboy_req:body(CowboyReq),
    case catch mb_util:json_decode(Body) of
        #{<<"imdbId">> := ImdbId, <<"screenId">> := ScreenId} ->
            Resp = mb_screen:seat_reserve(ScreenId, ImdbId),
            CowboyReq2 = cowboy_req:set_resp_body(mb_util:json_encode(Resp), CowboyReq1),
            {true, CowboyReq2, State};
        _ ->
            {false, CowboyReq1, State }
    end.

-spec handle_get(cowboy_req:req(), state()) ->
    {iodata(), cowboy_req:req(), state()}.
handle_get(CowboyReq, State) ->
    {QueryParams, _} = cowboy_req:qs_vals(CowboyReq),
    #{<<"imdbId">> := ImdbId, <<"screenId">> := ScreenId} = maps:from_list(QueryParams),
    {mb_util:json_encode(mb_screen:get(ScreenId, ImdbId)), CowboyReq, State}.
