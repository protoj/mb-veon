-module(mb_base_handler).
-include("mb.hrl").

-export([
    init/3,
    rest_init/2,
    allowed_methods/2,
    resource_exists/2,
    content_types_accepted/2,
    content_types_provided/2
]).

-type state() :: #{}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Cowboy Callbacks
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% @doc Upgrades to cowboy_rest.
%%      Basically, just returns <code>{upgrade, protocol, cowboy_rest}</code>
%% @see cowboy_rest:init/3
-spec init({atom(), atom()}, cowboy_req:req(), #{}) ->
    {upgrade, protocol, cowboy_rest}.
init(_Transport, _Req, _Opts) ->
    {upgrade, protocol, cowboy_rest}.

%% @doc Announces the Req and moves on.
%%      If <code>verbose := true</code> in <code>Opts</code> for this handler
%%      prints out a line indicating that endpoint that was hit.
%% @see cowboy_rest:rest_init/2
-spec rest_init(cowboy_req:req(), #{}) ->
    {ok, cowboy_req:req(), state()}.
rest_init(Req, Opts) ->
    {ok, Req, #{opts => Opts}}.

%% @doc Retrieves the list of allowed methods from Trails metadata.
%%      Parses the metadata associated with this path and returns the
%%      corresponding list of endpoints.
%% @see cowboy_rest:allowed_methods/2
-spec allowed_methods(cowboy_req:req(), state()) ->
    {[binary()], cowboy_req:req(), state()}.
allowed_methods(Req, State) ->
    #{path := Path} = maps:get(opts, State),
    Trail = trails:retrieve(Path),
    Metadata = trails:metadata(Trail),
    Methods = [atom_to_method(Method) || Method <- maps:keys(Metadata)],
    lager:notice("allowed_methods: ~p ", [Methods]),
    {Methods, Req, State}.

%% @doc Returns <code>false</code> for POST, <code>true</code> otherwise.
%% @see cowboy_rest:resource_exists/2
-spec resource_exists(cowboy_req:req(), state()) ->
    {boolean(), cowboy_req:req(), state()}.
resource_exists(Req, State) ->
    {Method, Req1} = cowboy_req:method(Req),
    {Method =/= <<"POST">>, Req1, State}.

%% @doc Always returns "application/json *" with <code>handle_post</code>.
%% @see cowboy_rest:content_types_accepted/2
%% @todo Use swagger's 'consumes' to auto-generate this if possible
%%       <a href="https://github.com/inaka/sumo_rest/issues/7">Issue</a>
-spec content_types_accepted(cowboy_req:req(), state()) ->
    {[{{binary(), binary(), '*'}, atom()}], cowboy_req:req(), state()}.
content_types_accepted(Req, State) ->
    #{path := Path} = maps:get(opts, State),
    {Method, Req2} = cowboy_req:method(Req),
    try
        Trail = trails:retrieve(Path),
        Metadata = trails:metadata(Trail),
        AtomMethod = method_to_atom(Method),
        #{AtomMethod := #{consumes := Consumes}} = Metadata,
        Handler = compose_handler_name(AtomMethod),
        RetList = [{iolist_to_binary(X), Handler} || X <- Consumes],
            lager:notice("content_types_accepted: ~p ", [RetList]),
        {RetList, Req2, State}
    catch
        _:_ -> {[{{<<"application">>, <<"json">>, '*'}, handle_post}], Req, State}
    end.

%% @doc Always returns "application/json" with <code>handle_get</code>.
%% @see cowboy_rest:content_types_provided/2
%% @todo Use swagger's 'produces' to auto-generate this if possible
%%       <a href="https://github.com/inaka/sumo_rest/issues/7">Issue</a>
-spec content_types_provided(cowboy_req:req(), state()) ->
    {[{binary(), atom()}], cowboy_req:req(), state()}.
content_types_provided(Req, State) ->
    #{path := Path} = maps:get(opts, State),
    {Method, Req2} = cowboy_req:method(Req),
    try
        Trail = trails:retrieve(Path),
        Metadata = trails:metadata(Trail),
        AtomMethod = method_to_atom(Method),
        #{AtomMethod := #{produces := Produces}} = Metadata,
        Handler = compose_handler_name(AtomMethod),
        RetList = [{iolist_to_binary(X), Handler} || X <- Produces],
        lager:notice("content_types_provided: ~p ", [RetList]),
        {RetList, Req2, State}
    catch
        _:_ -> {[{<<"application/json">>, handle_get}], Req, State}
    end.

-spec atom_to_method(get|patch|put|post|delete) -> binary().
atom_to_method(get) -> <<"GET">>;
atom_to_method(patch) -> <<"PATCH">>;
atom_to_method(put) -> <<"PUT">>;
atom_to_method(post) -> <<"POST">>;
atom_to_method(delete) -> <<"DELETE">>.

-spec method_to_atom(binary()) -> atom().
method_to_atom(<<"GET">>) -> get;
method_to_atom(<<"PATCH">>) -> patch;
method_to_atom(<<"PUT">>) -> put;
method_to_atom(<<"POST">>) -> post;
method_to_atom(<<"DELETE">>) -> delete.

-spec compose_handler_name(get|patch|put|post) -> atom().
compose_handler_name(get) -> handle_get;
compose_handler_name(put) -> handle_put;
compose_handler_name(patch) -> handle_patch;
compose_handler_name(post) -> handle_post.
