%%%-------------------------------------------------------------------
%% @doc mb top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(mb_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================
-spec start_link() -> {ok, pid()}.
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================

-spec init(list()) ->  {ok, {SupFlags :: supervisor:sup_flags(), [supervisor:child_spec()]}} | ignore.
init([]) ->
    {ok, { {one_for_all, 0, 1}, []} }.

%%====================================================================
%% Internal functions
%%====================================================================
