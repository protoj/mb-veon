-module(mb_screen).
-include("mb.hrl").

-export([new/3]).
-export([seat_reserve/2]).
-export([get/2]).

-spec new(binary(), binary(), integer()) -> #{}.
new(ScreenID, ImdbID, AvailableSeats) ->
    case mb_imdb:fetch_info(ImdbID) of
        {ok, #{<<"Title">> := Title}} ->
            {atomic, ScreenRec} = mnesia:transaction(fun() ->
                    case mnesia:read(mb_screen, ScreenID) of
                        [#mb_screen{} = Rec] -> Rec;
                        [] ->
                            Rec = #mb_screen{
                                screen_id = ScreenID,
                                imdbId = ImdbID,
                                movieTitle = Title,
                                availableSeats = AvailableSeats,
                                reservedSeats = 0
                            },
                            mnesia:write(Rec),
                            Rec
                    end
                end),
            mb_util:record_to_map(record_info(fields, mb_screen), ScreenRec);
        {ok, Error} ->
            format_error(Error);
        {error, request_failed} ->
            format_error(<<"wrong_imdbid">>)
    end.

-spec seat_reserve(binary(), binary()) -> #{}.
seat_reserve(ScreenId, ImdbID) ->
    {atomic, Res} = mnesia:transaction(fun() ->
            case mnesia:read(mb_screen, ScreenId) of
                [#mb_screen{imdbId = ImdbID} = ScreenRec] ->
                    ScreenRec1 = ScreenRec#mb_screen{
                        reservedSeats = ScreenRec#mb_screen.reservedSeats + 1,
                        availableSeats = ScreenRec#mb_screen.availableSeats - 1
                    },
                    if
                        ScreenRec1#mb_screen.availableSeats < 0 ->
                            format_error(<<"no_seats_available">>);
                        true ->
                            mnesia:write(ScreenRec1),
                            mb_util:record_to_map(record_info(fields, mb_screen), ScreenRec1)
                    end;
                _ ->
                    format_error(<<"screen_not_found">>)
            end
        end),
    Res.

-spec get(binary(), binary()) -> #{}.
get(ScreenId, ImdbID) ->
    {atomic, Res} = mnesia:transaction(fun() ->
            case mnesia:read(mb_screen, ScreenId) of
                [#mb_screen{imdbId = ImdbID} = ScreenRec] ->
                    mb_util:record_to_map(record_info(fields, mb_screen), ScreenRec);
                _ ->
                    format_error(<<"screen_not_found">>)
            end
        end),
    Res.

format_error(Reason) ->
    #{status => error, reason => Reason}.
