-module(mb_imdb).

%% API
-export([fetch_info/1]).

-define(IMDB_URL(QString), "http://www.omdbapi.com/?" ++ QString).
-define(IMDB_TEST_API_KEY, "plzBanMe").

-spec fetch_info(binary()) -> {ok, #{}} | {error, request_failed}.
fetch_info(ImdbID) ->
    QParams = lists:flatten(["i=", binary_to_list(ImdbID), "&apikey=", application:get_env(mb, imdb_api_key, ?IMDB_TEST_API_KEY)]),
    IMDBURL = ?IMDB_URL(QParams),
    lager:notice("imdb url: ~p", [IMDBURL]),
    case httpc:request(IMDBURL) of
        {ok, {{_HttpVer, 200, _StatMsg}, _Headers, Body}} ->
            {ok, mb_util:json_decode(Body)};
        Other ->
            lager:warning("IMDB info fetching error: ~p", [Other]),
            {error, request_failed}
    end.
