-module(mb_util).

-include("mb.hrl").
%% API
-export([json_encode/1, json_decode/1, record_to_map/2, secret/1]).

-spec json_encode(#{}) -> binary().
json_encode(JsonTerm) ->
    jiffy:encode(JsonTerm).

-spec json_decode(binary()) -> #{}.
json_decode(JsonB) ->
    jiffy:decode(JsonB, [return_maps]).

-spec record_to_map(list(), tuple()) -> #{}.
record_to_map(RecordFields, RecTuple) ->
    maps:from_list(lists:zip(RecordFields, tl(tuple_to_list(RecTuple)))).

%% @doc generates a secure randmo string
-spec secret(non_neg_integer()) -> binary().
secret(Size) ->
    binary:replace(
        base64:encode(crypto:strong_rand_bytes(Size)),
        [<<$/>>, <<$=>>, <<$+>>], <<>>, [global]).
