%%%-------------------------------------------------------------------
%% @doc mb public API
%% @end
%%%-------------------------------------------------------------------

-module(mb).
-include("mb.hrl").
-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).
%%-export([start/0, stop/0]).
-export([start_phase/3]).
%%====================================================================
%% API
%%====================================================================

%%====================================================================
%% Internal functions
%%====================================================================

-spec start_phase(atom(), application:start_type(), []) -> ok | {error, _}.
start_phase(create_schema, _StartType, []) ->
    _ = application:stop(mnesia),
    Node = node(),
    case mnesia:create_schema([Node]) of
        ok -> ok;
        {error, {Node, {already_exists, Node}}} -> ok
    end,
    mnesia:add_table_index(mb_screen, imdbId),
    {ok, _} = application:ensure_all_started(mnesia),
    mnesia:create_table(mb_screen, [
        {disc_copies, [node()]},
        {type, set},
        {attributes, record_info(fields, mb_screen)}
    ]),
    ok;

start_phase(start_cowboy_listeners, _StartType, []) ->
    Port = application:get_env(mb, http_port, 8000),
    ListenerCount = application:get_env(mb, http_listener_count, 10),

    Handlers =
        [
            mb_screen_handler,
            cowboy_swagger_handler
        ],
    Routes = trails:trails(Handlers),
    trails:store(Routes),
    Dispatch = trails:single_host_compile(Routes),

    TransOpts = [{port, Port}],
    ProtoOpts = [{env, [{dispatch, Dispatch}, {compress, true}]}],
    case cowboy:start_http(mb_server, ListenerCount, TransOpts, ProtoOpts) of
        {ok, _} -> ok;
        {error, {already_started, _}} -> ok
    end.

-spec start(StartType :: application:start_type(), StartArgs :: term()) ->
    {'ok', pid()} | {'ok', pid(), State :: term()} | {'error', Reason :: term()}.

-spec stop(State :: term()) ->
    term().

start(_StartType, _StartArgs) ->
    mb_sup:start_link().

%%--------------------------------------------------------------------
stop(_State) ->
    ok.
