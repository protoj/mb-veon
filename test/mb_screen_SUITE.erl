
-module(mb_screen_SUITE).


-include_lib("eunit/include/eunit.hrl").
-include_lib("common_test/include/ct.hrl").
-include("ct_http.hrl").


%% API
-compile(export_all).

init_per_suite(Config) ->
    ct_helper:start([mb]),
    Config.

end_per_suite(Config) ->
    application:stop(mb),
    Config.

init_per_testcase(_, Config)->
    Config.

end_per_testcase(_TestCaseName, _Config)->
    _Config.

all()-> [
    new,
    new_wrong_imdbid,

    get,
    get_wrong_screen_id,
    get_wrong_imdb_id

%%    reserve_seat,
%%    reserve_seat_wrong_screen_id,
%%    reserve_seat_wrong_imdb_id

].

-define(VALID_IMDBID, <<"tt0111160">>).
-define(INVALID_IMDBID, mb_util:secret(9)).
-define(JSON_HEADERS, [
    {"Content-Type", "application/json"}
]).
-define(URL(URI), "http://127.0.0.1:8585/" ++ URI).


new_screen(Config) ->
    Res = do_new(Config),
    mb_util:json_decode(Res#etest_http_res.body).

do_new(Config) ->
    AvailableSeats = 100,
    Result = ?perform_post(?URL("screen"), ?JSON_HEADERS, mb_util:json_encode(#{
        imdbId => proplists:get_value(imdbId, Config, ?VALID_IMDBID),
        screenId => proplists:get_value(screenId, Config, mb_util:secret(9)),
        availableSeats => proplists:get_value(availableSeats, Config, AvailableSeats)
    })),
    Result.

new(Config) ->
    Result = do_new(Config),
    ?assert_status(200, Result).

new_wrong_imdbid(Config) ->
    Result = new_screen([{imdbId , <<"wrong_id">>} | Config]),
    ?assertEqual(<<"error">>, maps:get(<<"status">>, Result, <<>>)).

get(Config) ->
    ImdbId = <<"tt0111161">>,
    #{<<"screen_id">> := ScreenId} = new_screen([{imdbId , ImdbId} | Config]),
    Result = ?perform_get(?URL("screen"), ?JSON_HEADERS, [{"imdbId", ImdbId}, {"screenId", ScreenId}]),
    ?assert_status(200, Result).

get_wrong_screen_id(Config) ->
    ImdbId = <<"tt0111162">>,
    #{<<"screen_id">> := _ScreenId} = new_screen([{imdbId , ImdbId} | Config]),
    Result = ?perform_get(?URL("screen"), ?JSON_HEADERS, [{"imdbId", ImdbId}, {"screenId", mb_util:secret(10)}]),
    ?assertEqual(<<"error">>, maps:get(<<"status">>, mb_util:json_decode(Result#etest_http_res.body), <<>>)).

get_wrong_imdb_id(Config) ->
    ImdbId = <<"tt0111162">>,
    #{<<"screen_id">> := ScreenId} = new_screen([{imdbId , ImdbId} | Config]),
    Result = ?perform_get(?URL("screen"), ?JSON_HEADERS, [{"imdbId", mb_util:secret(9)}, {"screenId", ScreenId}]),
    ?assertEqual(<<"error">>, maps:get(<<"status">>, mb_util:json_decode(Result#etest_http_res.body), <<>>)).